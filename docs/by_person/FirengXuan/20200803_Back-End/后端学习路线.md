# 后端学习路线

## 前端

### html

### css

### js

### jquery

### ajax

### json

### 小程序

### node.js

### vue

### webpack

## 后端

### db

- mysql
- oralce
- sqlserver
- no-sql

	- redis
	- mongodb
	- memcache

- 中间层

### java

- java se
- java web

	- 单体

		- servlet
		- jsp等模板引擎
		- tomcat
		- spring
		- springmvc
		- structs
		- orm

			- hibernate(SSH)

				- spring data jpa

			- mybatis(SSM)

				- Mybatis-plus
				- tkMybatis

		- springboot

	- 微服务

		- spring cloud(社区较活跃)

			- 注册中心

				- Eureka
				- ZooKeeper
				- Consul
				- nacos

			- 服务调用

				- Ribbon
				- LoadBalancer
				- Feign
				- OpenFeign

			- 服务降级

				- hystrix
				- sentinel

			- 服务网关

				- zuul
				- gateway

			- 服务配置

				- config
				- nacos

			- 服务总线

				- bus
				- nacos

		- dubbo

- jvm
- 设计模式

### mq(消息队列)

- RabbitMQ
- Kafka
- RocketMQ
- ActiveMQ

### 算法

### 计算机网络

## linux

### linux基础

### nginx

### docker

### k8s

## 阶段三

## 阶段一

## 阶段二

## 阶段四