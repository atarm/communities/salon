---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Reopening_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Reopening

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [About the Salon](#about-the-salon)
2. [Why the Salon](#why-the-salon)
3. [How the Salon](#how-the-salon)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## About the Salon

---

非官方、非盈利、价值主题、集市风格、君子之交、年龄无关的Salon Community...

---

1. 2020/07/20：aRoming启动分享
1. 2020/07/27：StringLiu（刘使峻）-前端技术
1. 2020/08/03：FirengXuan（禤志涛）-后端技术
1. 2020/08/13：AC1128（邱福灿）-PM的岗位简述和个人见解
1. 2020/08/17：ExecutorCheng（程济中）-CSharp and Team-Work
1. 2021/08/09：StringLiu（刘使峻）-从0到1的业务流程与个人成长-如何有效的工作拧螺丝，面试造火箭

---

### 鸣谢赞助

![](./.assets/image/sponsor.png)

---

赞助商邀约中。。。

感谢对本Salon的贡献和支持。。。

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Why the Salon

---

1. 为什么要开始？
1. 为什么中间会暂停？
1. 为什么要重启？
1. 能不能持续下去？

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## How the Salon

---

### The Thought

1. 过程资产、持续改进
1. 倾听经验、节约时间
1. 集市风格、君子之交

<!--failure must early-->

---

![height:620](./.assets/image/the_cathedral_and_the_bazaar.jpeg)

---

### The Action

1. 乐于分享
1. 空杯心态
1. 不吝反馈
1. 善用佳软
1. 持续改进

**it doesn't matter the salon or seminar or other form...just sharing...**

---

![height:640](./.assets/image/_The_School_of_Athens__by_Raffaello_Sanzio_da_Urbino.jpg)

---

### Socratic

I know that I know nothing...

---

It is also a paradox...

---

### Arrange

1. Time：尽可能公约的课后时间，相对固定的一个时间
1. Place：线上，1小时左右（45分钟分享、15分钟交流）
1. People：赞同“集市风格、君子之交”的同仁们
1. Event：任意自己感兴趣的、对他人有价值的、经思考整理的主题，如：一个技术、一本书、一次经历、一个感想

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗

**...To Be Continue...**
