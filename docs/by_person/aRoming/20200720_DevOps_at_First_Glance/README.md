---
marp: true
theme : gaia
class : invert
size: 16:9
auto-scaling: true
paginate: true
backgroundColor: #202228
header: "DevOps at First Glance"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_class:
    - lead
-->

# DevOps at First Glance

###### The Modern Software Engineering We Should Use As Early As Possible

---

## About This Salon

1. **who**
    1. **presenter** : @aRoming
    1. **participant** : teammates
1. **when** : 2020/07/20 09:00 AM
1. **where** : Tencent Meeting (Meeting ID: 605 595 200)
1. **why** : _**make common sense**_, _**collaborate without dirty work**_
1. **how** : feel free to discuss
1. **topic** : DevOps at First Glance

---

## About This Presentation Writing Environment

1. Editor: [VS Code](https://code.visualstudio.com/)
1. Language: `Markdown`
1. Plugin: [Marp for VS Code](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode)

:point_right: <https://gitlab.com/arm_courses/sr/-/tree/master/Expertiments/Expt_Guides/SR-E02_Guide>

---

## Why in English?

1. my English skill is so poor that should learn hard
1. newest and most Important in IT almost all in English
1. English will less garbled character and less trouble in many times
1. so many times skip the prompts in English, even it is so clear
1. we code in English

---

## Agenda: LML, VCS

1. reconsider `SDLC`
1. what is `LML`? why use `LML`? what is `markdown`? why use `markdown`?
1. what is `VCS`? why use `VCS`? what is `git`? why use `git`?
1. what is `github`? what is `gitlab`? why `gitlab`?

---

## Agenda: Cloud and Container

1. what is `container`? why use `container`? what is `docker`? why use `docker`?
1. what is difference between `VM` and `container`? will `container` replace the `VM`?
1. what is `cloud`/`IAAS` and what is `cluster`? what is `orchestration`?

---

## Resource

### :question: how to learn `markdown`, `git` and `docker`? where is the learning resources?

1. <https://gitlab.com/arm_awesomes>
1. <https://gitlab.com/arm_courses/sr>
1. <https://pan.baidu.com/s/1kzyHsVeNESh1QKA9elzmaw>, PASSWD: ke3p

---

## Our Most Important Objects

1. _**culture**_ : sharing && collaborate
1. _**documentation**_ : versionable && evolvable
1. _**workflow**_ : efficient && monitorable

---

## Preface

1. why should never fail in course
1. principle
    1. _**everything is for construct efficiently**_
    1. _**the only thing i know is that i know nothing**_
1. [8 Hacks For Your Next Tech Resume](https://dev.to/gemography/common-mistakes-in-dev-cvs-2a17)
1. terms relative: _**engineering**_, _**architecture**_, _**salon**_

---

## About Us

1. we are _**the team**_ beyond _**teacher - student**_
1. we are opening _**salon**_, not _**class**_

---

## About Our Salon

1. _**we should talk about**_
    1. catalog of technique and resource
    1. technique trend, popular and breakthrough
    1. learning/project experience and thoughts
    1. anything interesting and meaningful
1. _**we should not talk about**_
    1. duplicate lessons in course
    1. all the minor details, has no information

---

## PDCA: Plan - Do - Check - Adjust

:point_right: so many things look like the same inner deep...

---

## SDLC: Software Development Life Cycle

1. _**inception**_ : planning, defining
1. _**elaboration**_ : designing
1. _**construction**_ : coding, building, testing, integrating
1. _**transition**_ : deploying, maintaining

:point_right: <https://about.gitlab.com/direction/maturity/>

---

## LML: Lightweight Markup Language

:point_right: <https://gitlab.com/arm_awesomes/writing/-/tree/master/01_LML>

:point_right: <https://hyperpolyglot.org/lightweight-markup>

:point_right: every programmer and internet product manager should know and use...

---

## VCS: Version Control System

1. `SCM`: in `VCS` it is short of Source Control Management, other scenarios it maybe is short of Software Configuration Management
1. `non-linear editing`

:point_right: <https://gitlab.com/arm_awesomes/devops/-/tree/master/01_VCS>

---

## Workflow

>**Han Xin's soldiers - the more the better**

>**manage flow, not people!**  
>a system is never the sum of its parts. It is the product of the interactions of its parts  
>from: <https://www.agile42.com/en/blog/2020/04/17/manage-flow-not-people/>

:point_right: <https://gitlab.com/arm_awesomes/devops/-/blob/master/05_Platform/01_Git-based_Workflow/02_github_flow.md>

---

## VM and Container

1. `container` is not `virtual machine`
1. `container` can work with `virtual machine`, and make it better

:point_right: <https://gitlab.com/arm_awesomes/devops/-/tree/master/02_Container>

---

## IAAS and Cluster

1. `IAAS`: Infrastructure As A Service
1. `DAAS`: Data As A Service
1. `CAAS`: Container As A Service
1. `XAAS`: X As A Service
1. `cluster`: many machines do one work

:point_right: <https://gitlab.com/arm_awesomes/infrastructure/-/tree/master/05_Cluster>

---

<!--
_backgroundColor: FloralWhite
_class:
    - lead
-->

## Thank You for Listening ...

### Feel Free to Discuss ...
