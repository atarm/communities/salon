# Java后端学习路线

## [Java基础](https://www.liaoxuefeng.com/wiki/1252599548343744)

1. 面向对象 🌟 👍
1. 多线程 🌟 👍
1. 接口 👍
1. 枚举
1. 集合
1. String,StringBuffer,StringBuilder
1. 日期时间类
1. 异常处理
1. IO流
1. 反射

## 前端基础

1. html,css,js
    + html:<https://www.bilibili.com/video/BV1YJ411a7dy>
    + css:<https://www.bilibili.com/video/BV1YJ411a7dy>
    + js:<https://www.bilibili.com/video/BV1JJ41177di>
1. [jQuery](https://www.bilibili.com/video/BV1ts411E7ag?p=1)
1. [Ajax](https://www.bilibili.com/video/BV1Kt411u7BV?from=search&amp;seid=679271268810815946&amp;spm_id_from=333.337.0.0)

## 数据库

1. [mysql](https://www.bilibili.com/video/BV1NJ411J79W)
    + sql语句CRUD编写
    + 设计数据库表
    + 索引
    + 事务
    + 锁机制
    + 性能优化
1. MongoDB
    + MongoTemplate的CRUD

## [JavaWeb](https://www.bilibili.com/video/BV12J411M7Sj?from=search&amp;seid=6142831876263952246&amp;spm_id_from=333.337.0.0)

1. XML
1. JSON
1. Servlet
1. Filter
1. Listener
1. JSP
1. Cookie
1. Session
1. Maven

## SSM框架

1. [Spring5](https://www.bilibili.com/video/BV1WE411d7Dv?from=search&amp;seid=17021545658765945624&amp;spm_id_from=333.337.0.0)
    + IOC
    + AOP
    + 事务
1. [SpringMVC](https://www.bilibili.com/video/BV1aE41167Tu?from=search&amp;seid=17021545658765945624&amp;spm_id_from=333.337.0.0)
    + 请求与响应
    + Restful API
    + 拦截器
    + 配置
    + 执行过程
1. [Mybatis](https://www.bilibili.com/video/BV1NE411Q7Nx?from=search&amp;seid=17021545658765945624&amp;spm_id_from=333.337.0.0)
    + CRUD
    + 动态 SQL

## 大前端

1. [Vue || React](https://www.bilibili.com/video/BV1Zy4y1K7SH?from=search&amp;seid=3903282548667203481&amp;spm_id_from=333.337.0.0)
1. ES6
1. npm
1. Babel
1. axios
1. nodejs
1. webpack

## 中间件

1. [redis](https://www.bilibili.com/video/BV1S54y1R7SB?from=search&amp;seid=6007835216252877411&amp;spm_id_from=333.337.0.0)
1. es
1. mq
1. git

## 微服务

1. [SpringBoot](https://www.bilibili.com/video/BV1PE411i7CV)
    + [前后端分离练手项目](https://www.bilibili.com/video/BV1PQ4y1P7hZ?from=search&amp;seid=10011672735803763960&amp;spm_id_from=333.337.0.0)
1. [Mybatis-Plus](https://www.bilibili.com/video/BV17E411N7KN)
1. [SpringSecurity || shiro](https://www.bilibili.com/video/BV1KE411i7bC)
1. [SpringCloud](https://www.bilibili.com/video/BV18E411x7eT)
    + SpringCloudAlibaba
        + Nacos
        + OpenFeign
        + Hystrix
        + GateWay

## 运维

1. Linux基础
1. [Nginx](https://www.bilibili.com/video/BV1F5411J7vK?from=search&amp;seid=2770544568724977312&amp;spm_id_from=333.337.0.0)
    + 反向代理（负载均衡）
    + 动静分离
1. [Docker](https://www.bilibili.com/video/BV1og4y1q7M4?from=search&amp;seid=8653466887348399378&amp;spm_id_from=333.337.0.0)
    + 容器概念
    + 镜像
    + 部署服务
    + Dockerfile
    + Docker Compose
1. Jenkins
    + DevOps

## 第三方应用

1. POI
1. 短信接口
1. 视频点播
1. 视频直播
1. 第三方登录
1. 第三方支付

## 源码探究

1. 23种设计模式
1. spring源码

## 计算机基础知识

1. 计算机网络
1. 操作系统
1. 数据结构
