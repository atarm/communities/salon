# 前端工程师入门线路[预计18个月]

## 前端技能[从上往下学，学完一个大体系就要实战巩固知识，预计15个月]

### 前端基础知识[预计最多两个月]

#### html[预计15天]

##### 学会基本的html标签、属性的使用

##### html5相关新增标签、nav、header、footer、video、audio等

##### html5相关canvas的理解、history的基本使用（学习完JavaScript再来）

##### html5相关的localStorage、sessionStorageAPI的基本使用

#### css[预计12天]

##### 学会基本的css相关知识、类、ID等选择器的使用

##### css相关的伪类选择器、属性选择器、	伪元素选择器、	通配选择器、兄弟选择器等

##### css3相关transition简单动画、animation复杂动画

##### css3相关的transform转化、filter过滤、径向和线性渐变、box-shodow、text-showdow等

##### css3相关的布局、flex布局、gird布局的使用、双飞翼布局、BFC、假的等高、css规范

#### javascript[预计20天]

##### 学会es5之前的基本的JavaScript知识、dom、bom的简单运用、计时器、定时器的使用

##### 学习JavaScript的this指向问题、闭包问题、var的问题、原型和原型链的理解

##### 学习JavaScript类的实现、了解圣杯模式是什么，闭包、this、var这些问题出现在什么地方

##### 学习JavaScript相关的ajax请求方式，ajax的基本使用、fetchAPI的请求方式和基本使用

##### 学习JavaScript的一些新特性、就是ECMAScript相关的知识

##### 深入学习:学习JavaScript的无副作用函数

#### ECMAScript2015[es6][预计5天]

##### es6新特性的let、const定义

##### ...解构运算符

##### 数组相关新增map、filter、find、findIndex、includes等方法

##### 对象相关新增keys、values、create、assign等方法

##### 对象和数组解构赋值的使用

##### Map、List的一些简单使用

##### Promise、async\await等回调地狱解决方案处理

##### 还有一些其他新特性、具体看学习资料介绍

##### 终极目标:对Promise的源码进行书写理解

#### http协议、请求状态码[预计2天]

##### 了解http的工作原理

##### 了解掌握http请求状态码的对应意思、以及一些请求头的概念

### 前端中间过度知识[预计十天任务]

#### jquery[预计5天]

##### jquery的一些基本使用，相对原生的简写方式，不用精通、了解会用即可

#### bootstrap[预计2天]

##### bootstrap的一些基本使用、懂的使用class名进行样式提供，了解会用即可

#### 对所学知识进行实战、两次以及以上、便于理解和使用

### 前端框架知识[预计最多五个月]

#### less,scss[预计5天]

##### 掌握less的基本使用和操作

##### 掌握scss的基本使用和操作

#### Vue.js[预计一个月任务]

##### Vue知识的入门学习、了解MVVM的含义、Vue为什么会出现？Vue出来解决了啥

##### Vue的基本使用，el、mount挂载、template、双大括号模板、data方法\data对象的用途

##### Vue组件注册的作用域、全局组件和局部组件，理解vue相关的一些响应式数据原理等

##### Vue的基本指令v-if、v-show、v-for、v-model、v-on:xxxx、v-bind:xxxx以及后面两个的简写

##### Vue的生命周期函数、八大基本生命周期函数、牢记生命周期函数的调用时机，掌握ref的使用

##### Vue对象上methods、computed、watch这三个对象、Prop的父组件传子组件的使用

##### Vue的slot插件的作用域和使用、Vue的自定义事件和异步组件keep-alive以及对应的两个生命周期函数

##### 深入Vue的动画transition组件、mixin混入、filter过滤器、渲染函数render和自定义指令

##### 理解vue.config.js文件配置的代理解决跨域问题

##### 学习Vue的路由库vue-router库的学习、掌握router-view、link-view组件和路由配置、hash和history的区别等

##### vue-router库的router的生命周期、$route、$router对象的理解、以及路由守卫等

##### 学习vuex状态管理、学会编写state、getters、mutations、actions和modules、以及commit、dispatch方法

##### 学习vue相关的UI库、例如ElementUI、Vant等UI库，要运用到实战中使用

##### 对所学知识进行实战、两次以及以上、便于理解和使用

#### Node.js[预计一两个月任务]

##### node基础知识[预计5天]

###### 了解node.js的起源、出现、好处与弊端

###### 掌握http模块建立http服务器，开发后端接口

###### 掌握fs的读写文件、流式读写、判断是否是文件夹等操作

###### 掌握path模块的路径拼接、路径获取、路径分解等使用，已经其他一些模块的使用

##### 数据库mongodb、mysql在node的使用[预计5天]

###### 学习mongodb数据的基本的安装和增删改查

###### 学习mongodb结合连接express框架、koa框架进行对数据库的增删改查操作

###### 学习mysql的基本增删改查操作

###### 学习运用mysql模块连接express框架、koa框架进行增删改查操作

##### express框架[预计12天]

###### 掌握express框架的路由、中间件的创建和使用，以及多级路由的配置

###### 掌握express框架对post、delete、put请求的请求体的body-parser的配置

###### 掌握express框架的文件上传下载等操作[multer库、fs模块、path模块的结合使用]

###### 掌握express框架对静态文件获取、文件转码、跨域配置的使用方式

###### 掌握express结合mongodb数据库的连接操作增删改查[使用mongoose]

###### 对express框架进行接口实战学习

##### koa框架[预计10天]

###### 掌握koa框架的中间件使用和配置中间件

###### 掌握koa-router配置的路由中间件，并懂的使用koa+koa-router创建路由接口

###### 掌握koa相关的文件上传下载等操作[multer库、fs模块、path模块的结合使用]

###### 掌握koa+koa-router+koa-body-parser的结合使用，允许跨域并编写接口

###### 掌握koa相关和mongodb连接的操作，运用mongoose对数据库进行增删改查

###### 对所学的koa框架进行结合其他东西编写接口实战

###### 对koa相关知识点进行接口实战学习

##### webpack基础知识[预计10-15天]

###### 掌握webpack出现的原因、解决什么问题、使用webpack带来什么好处和弊端

###### 掌握webpack的mode、devtool、module、plugins、devServer、resolve、context、entry、output的一些基本使用

###### 掌握webpack结合其他babel-loader,css-loader,less-loader,file-loader等的结合使用

###### 掌握webpack结合clean-webpack-plugin,webpack自身插件，html-webpack-plugin等插件的使用

###### 解读和搭建vue的编译相关配置、解读和搭建react的编译相关配置

#### React.js[预计两个月]

##### react入门[预计10天]

###### React的出现，React是MVC框架、React的优点和缺点、全球最牛逼的前端框架React.js的用法等

###### React的jsx语法编写，state状态设置，props的子父组件传递，方法传递等

###### React的类组件和函数组件的区别，类组件的生命周期函数

###### React的事件处理、条件渲染、map和key的循环渲染的使用

###### React的状态提升、React开发的哲学解读

###### React的Ref的使用，ref对dom的操作使用，或者使用ref存储不可变数值

###### React的类组件的this指向问题，在哪些生命周期函数中请求接口并返回，哪些生命周期函数中操作dom、绑定window事件等

##### react进阶[预计40-50天]

###### 理解React的Fragments标签解决的问题，掌握啥是非受控组件

###### 理解React的ContextAPI的使用，Context在哪些地方可以使用

###### 理解React的错误边界，如何捕抓组件产生的错误

###### 理解React的高阶组件的使用和运用途径

###### 使用React的使用 PropTypes 类型检查，或者静态类型检查，严格检查props的类型

###### 使用react-router-dom库结合react书写路由组件，对HashRouter，BrowserRouter，Switch，Route，Link，NavLink，Prompt等组件的使用和理解

###### 使用redux库结合react，如何书写出具备中央状态管理的react项目，理解redux出现解决的问题，flow流的流向

###### 使用redux库，灵活运用createStore，combineReducers、bindActionCreators的使用，懂的action、reducer，action-type的书写和编写

###### 使用redux库进行dispatch分发action，从而改变reducer来进行改变刷新state，同时对页面进行局部刷新

###### 使用redux+react-redux结合react进行高阶组件连接，connect连接使用

###### 使用redux+react-redux+redux-thunk编写异步action的方式，在action中发送请求并返回结果提供给组件

###### 使用redux+react-redux+redux-logger+redux-promise，理解和使用相关中间件的工作流程，对相关API进行书写和编写

###### 使用antd这个UI库结合react+redux+react-router组合进行简单的使用和配置，以及按需加载的文件配置

###### 理解React新API，用函数组件替代类组件的hooks的使用，懂的封装和使用hooks

###### 理解hooksAPI的useState、useEffect、useRef的简单使用

###### 使用react全家桶进行项目实战，了解react的项目使用

### 前端进阶知识[预计三个月]

#### Vue.js的深入[预计半个月]

##### 掌握和理解Vue.js的v-model语法糖的使用

##### 掌握和理解Vue相关的$parent、$children、$attrs等的使用常用

##### 掌握和理解Vue的Context实现，Provide、Inject的使用和理解

##### 掌握和理解Vue，封装属于自己的UI组件，通用组件的props的具体使用和理解

##### 掌握Vue的Vue.use方法的使用条件，编写自己可以提供相关使用的install

##### 掌握Vue的Object.definedProperty的响应式数据实现

##### 掌握Vue的Diff算法的简单实现，Vue的时间复杂度

##### 简单理解Vue的部分实现源码，了解Vue的工作流程

##### 了解Vue结合jest等测试框架的基本使用

#### 微信小程序的简单学习和使用[预计半个月]

##### 懂的微信小程序的简单配置，如何注册微信小程序开发者

##### 理解微信小程序的wxml，wxss，js文件的使用和理解

##### 理解微信小程序的编写和调试，掌握下面导航条的配置，懂的微信小程序的一些简单配置文件

##### 理解和掌握微信小程序的生命周期函数，如何渲染和请求数据

##### 理解和使用微信小程序API开发简单小程序项目

#### React的深入学习[预计半个月]

##### 理解和掌握React的性能优化，了解ref转发到函数组件内部的forward高阶组件的使用[函数组件无法绑定ref]

##### 掌握和使用React的PureComponent的解决的问题，掌握和使用函数组件的memo性能优化

##### 掌握和使用React的懒加载lazy，Suspense的结合使用，进行路由，组件懒加载

##### 掌握和使用ReactHooks的useCallback、useMemo的简单使用和解决的问题

##### 掌握和使用ReactHooks的useContext，useReducer替代Redux，自行实现redux功能的API

##### 掌握和使用immutable结合react进行代码编写，了解immutable的性能优化魅力所在

##### 了解React相关的jest测试的基本使用

##### 了解React相关的部分工作流程，解读Redux的一些简单实现

#### Typescript语言的学习[预计五天]

##### 理解Typescript的出现和解决的问题，懂的TS的弊端和优点

##### 掌握TS的各种数据类型，懂的定义相关的TS类型定义

##### 掌握TS的类、接口、泛型方法、泛型类、抽象类、命名空间等的使用

##### 掌握TS定义静态、private、public等的类，理解.d.ts文件的存在

##### 了解TS的tsconfig.json文件的一些参数和配置，如何编译程js代码并运行等操作

#### TS结合vue的开发和使用[预计半个月项目实战]

##### 使用vue+TS进行书写ts相关的vue代码，简单的类型定义和使用

##### 使用Vue+TS简单开发一个小型项目，理解Vue使用中的axios库，其他库的基本数据类型，接口类型

#### TS结合node的开发和使用[预计十天项目实战]

##### TS结合express开发接口，理解express中的类型定义，懂的如何把ts+express编译成正常的js项目

##### TS结合koa开发接口，理解koa中的类型定义，懂的如何把ts+koa编译成正常的js项目

#### TS结合React的开发和使用[预计半个月项目实战]

##### TS结合React开发泛型类，泛型组件，类型约束的组件，使用TS严格规范React的组件，数据类型

##### TS结合React进行真实项目开发，灵活运用相关redux、react-router-dom、antd的一些参数的数据类型

### 前端扩展知识[预计四个月]

#### Vue.js的多端解决方案uniapp[学习+实战二十天]

##### 理解uniapp出现，uniapp八端解决方案出现的问题

##### 使用uniapp开发小程序相关，h5页面相关，懂的px,rem,vw,vh等单位的具体意义

##### 使用uniapp开发简单APP，理解uniapp在多端解决的时候所面临的问题

#### Vue.js的ssr服务器渲染解决方案nuxtjs[学习+实战二十天]

##### ssr服务器渲染出现解决的问题

##### Vue的ssr服务器渲染解决方案nuxtjs的一些基本配置和使用，新增API，理解nuxt.config.js的配置文件

##### 了解Vue的nuxt.js服务器渲染框架结合TS的文件配置和使用

##### 懂的使用nuxt.js开发一个简单的服务器渲染项目，理解和掌握一些简单API的使用

#### React.js的多端解决方案taro[学习+实战二十天]

##### 理解taro出现所解决的问题，taro多端解决方案

##### 使用taro开发小程序相关，h5页面相关，懂的px,rem,vw,vh等单位的具体意义

##### 使用taro开发简单APP，理解taro在多端解决的时候所面临的问题

#### React.js的ssr服务器渲染解决方案nextjs[学习+实战二十天]

##### ssr服务器渲染出现解决的问题，react的解决方案next.js所带来的好处

##### next.js的一些基本配置和使用，懂的一些模板文件，新增API，理解next.config.js的配置文件

##### 了解React的next.js服务器渲染框架结合TS的文件配置和使用

##### 懂的使用next.js开发一个简单的服务器渲染项目，理解和掌握一些简单API的使用

#### React.js的redux和react-router-dom简化方案dva.js[学习两三天]

##### redux解决问题的同时所面临的问题，dva解决的问题

##### dva的一些简单使用，理解，知道路由，redux的使用

##### dva的一些简单实现原理，dva框架的优点和好处

#### React.js的APP开发框架ReactNative[入门到精通一个月]

##### 理解React-Native框架，理解编译Android，IOS的项目

##### 理解和掌握React-Native框架的一些基本组件，内置组件，如何兼容对应的两个APP端

##### 理解和掌握React-Native框架结合其他相关库进行APP编写

##### 使用React-Native开发一个简单的APP，对所学API和组件进行理解

## 算法和设计模式

### 算法相关知识-时间复杂度、空间复杂度分析、排序、查找等。

### 常用的设计模式-单例模式、工厂模式、观察者模式、装饰器模式、适配器模式、中介者模式等。

## 前端技能相关的技能[预计1个月之内]

### 了解并会简单使用linux的一些命令

### 了解并且会使用git的简单命令

### 了解和使用一些nginx的简单命令

## 后端技能[预计两个月之内]

### JAVA以及框架

#### JAVA基础知识，面向对象三大特性

##### 封装

##### 继承

##### 多态

#### JAVA的各种类型、包装类、接口、抽象类

##### 八大包装类

##### 基本、引用数据类型

##### 抽象类、抽象方法

##### 接口、类的私有private\公共public

#### 其他一些知识

##### session、cookie

##### 泛型、servlet

#### SSM三大框架

##### spring框架的基本使用

##### springmvc框架的基本使用

##### mybatis框架相关基本使用

##### 了解aop、面向切面编程

### 了解Python基本语言知识

#### Python基本知识

## 其他技能

### PS基本知识(基本的会就差不多了)

### 了解一些IT方面概念(随便读)
