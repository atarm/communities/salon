## git 相关命令

***
### 配置git的ssh

- cd ~/.ssh  

- ssh-keygen -t rsa -C "2278765105@qq.com" [生成密钥]，然后去github的settings里面的SHH and GPG keys 添加,值就是本地密钥

***
### git命令集
命令 | 介绍
---- | ---
git init | 初始化本地git，生成.git文件
git remote rm origin | 取消与仓库的关联 ```(origin仓库的名字)```
git remote -v  | 查看和远程仓库有没有关联
git remote add origin url | 关联到仓库 ```(url就是仓库地址)```
... | ...
git clone url/ssh | 克隆项目到本地
git status | 查看添加或者修改的文件或者文件名 ```（没有提交之前）```
git diff | 查看修改的内容变化、也可以指定文件
git branch | 查看当前有什么分支
git log | 提交日志查看
git show logId | 展示某次提交的信息，```需要log的id```
git checkout filename/. | 撤销某个文件或者所有文件到```上一步```
git add ./filename | 提交所有或者单个文件到缓存区
git commit -m 'xxx' | 本次提交、信息是xxx
git push origin master | 把```文件/文件夹```push到git上面的master分支
git pull origin master | 把master分支的代码拉下来

---
### 多人开发的时候

命令 | 介绍
---- | ---
git checkout -b newBranchName | 切换一个新的```newBranchName```分支
git checkout master | 切换分支
git fetch | 拉取所有分支
git merge branch1 | 合并分支 ```(比如现在在master分支、输入这个命令，然后把branch1分支合并过来)```
git push origin –-delete branchName | 要删除服务器远端的分支，则执行如下所示的命令
git branch –D branchName | 如果是要删除本地已经合并了的分支

--- 
### 合并出现，分支冲突

- 修改代码、可以保存两者、也可以保存一个

--- 
### 不小心改错分支、例如在master分支```(这时候撤回浪费了，不要使用git checkout 全部撤回 浪费了)```

命令 | 介绍
---- | ---
git stash | 先把代码放到别的区域,修改的文件先放到一边、只有新增的文件 ```(暂存了修改和新增的文件)```
git checkout -b newBranch | 这时候创建新的分支，会把新增的直接放到新的分支中
git stash pop | 把暂时存储起来的修改文件吐出来