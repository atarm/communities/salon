// 拿到节点
const webSlides = document.querySelector('#webslides');

// 是否全屏？
let isFullScreen = false;
const fullSpan = document.querySelector('.full');

const drawOpenBtn = document.querySelector('.draw-open');

// 开始创建
createSlides(webSlides);

/**
 * 创建slides
 * @param {HTMLElement} container
 */
function createSlides(container) {
  /** @type {HTMLTableSectionElement} */
  let section;
  /** @type {HTMLImageElement} */
  let img;
  let i;
  for (i = 1; i <= 41; i++) {
    section = document.createElement('section');
    section.classList.add('aligncenter'); // 添加类名
    img = document.createElement('img');
    img.src = `./images/${i}.png`;
    img.draggable = false;
    section.appendChild(img);
    container.appendChild(section);
  }
  // 创建幻光片
  const ws = new WebSlides();
}

/**
 * 开启全屏模式
 * @param {HTMLElement} element 需要全屏的元素
 */
function launchFullScreen(element) {
  if (element.requestFullScreen) {
    element.requestFullScreen();
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.webkitRequestFullScreen) {
    element.webkitRequestFullScreen();
  }
}
/** 取消全屏模式，需要注意的是，cancelFullScreen只被文档对象调用，无需单个元素调用。 */
function cancelFullscreen() {
  if (document.cancelFullScreen) {
    document.cancelFullScreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitCancelFullScreen) {
    document.webkitCancelFullScreen();
  }
}

// document.fullScreenElement：当前全屏显示的元素。
// document.fullScreenEnabled：判断浏览器是否支持全屏。
// fullscreenchange事件：全屏状态改变事件。
// fullscreenchange事件要绑定在document上，该事件仅在全屏状态改变时触发，默认没有任何动作。
createListener(drawOpenBtn);

// 监听点击后的变化
fullSpan.addEventListener('click', () => {
  if (isFullScreen) {
    // 关闭全屏
    cancelFullscreen();
  } else {
    // 打开全屏，可以传递对应的dom节点实例
    launchFullScreen(document.documentElement);
  }
});
// 绑定监听全屏打开和关闭的变化
document.addEventListener('webkitfullscreenchange', () => {
  if (isFullScreen) {
    fullSpan.textContent = '全屏模式';
    isFullScreen = false;
  } else {
    fullSpan.textContent = '关闭';
    isFullScreen = true;
  }
});
