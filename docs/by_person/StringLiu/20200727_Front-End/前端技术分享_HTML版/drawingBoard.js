// get canvas label element
const myCanvas = document.querySelector('canvas#my-canvas');
// get 2d canvas and set ctx variable
const ctx = myCanvas.getContext('2d');
// get color input label element
const colorSelect = document.querySelector('.color-select');
const range = document.querySelector('.range');
const spanSize = document.querySelector('.size');
const closeDrawingBroad = document.querySelector('button.close');
const saveDrawingBoard = document.querySelector('button.save');
// 拿到画板全部
const canvasDraw = document.querySelector('.canvas-draw');
// default draw line width
let lineWidth = 2;
// default draw line color
let lineColor = colorSelect.value;
// set canvas label width and height to browser identical
let pageWidth, pageHeight;
handleCanvasSize();

// 绑定监听全屏打开和关闭的变化
document.addEventListener('webkitfullscreenchange', handleCanvasSize);
function handleCanvasSize() {
  pageWidth = document.documentElement.clientWidth;
  pageHeight = document.documentElement.clientHeight;
  myCanvas.width = pageWidth;
  myCanvas.height = pageHeight;
}

// start point object
let startPoint = { x: 0, y: 0 };

let painting = false; // 是否画
myCanvas.addEventListener('mousedown', (e) => {
  let x = e.offsetX; // get mouse x axis position
  let y = e.offsetY; // get mouse y axis position
  painting = true; // can be on drawn
  startPoint = { x, y }; // start point object
});
myCanvas.addEventListener('mousemove', (e) => {
  let x = e.offsetX; // get mouse x axis position
  let y = e.offsetY; // get mouse y axis position
  let newPoint = { x, y }; // end point object
  if (painting) {
    // can be on drawn to start draw line
    drawLine(startPoint.x, startPoint.y, newPoint.x, newPoint.y);
    startPoint = newPoint; // save end point object to start point object
  }
});
// listener mouse up event
myCanvas.addEventListener('mouseup', () => {
  painting = false; // can not draw
});
closeDrawingBroad.addEventListener('click', () => {
  ctx.clearRect(0, 0, pageWidth, pageHeight);
});
// draw line function
function drawLine(startX, startY, endX, endY) {
  ctx.beginPath(); // start draw
  ctx.moveTo(startX, startY); // start point
  ctx.lineTo(endX, endY); // end point
  ctx.lineWidth = lineWidth; // set line width
  ctx.strokeStyle = lineColor; // set line color
  ctx.stroke(); // end draw and show to canvas
}
// listener color input change
colorSelect.onchange = (e) => {
  lineColor = e.target.value; // get color
};
// listener range input change to set lineWidth size
range.onchange = (e) => {
  lineWidth = e.target.value;
  spanSize.textContent = lineWidth; // show line width to span label
};
// save drawing board to image
saveDrawingBoard.onclick = function () {
  let url = myCanvas.toDataURL('image/jpg');
  let a = document.createElement('a');
  document.body.appendChild(a);
  a.href = url;
  a.style.display = 'none';
  a.download = 'drawingBoard';
  a.target = '_blank';
  a.click();
  document.body.removeChild(a);
};
/**
 * 创建监听现实隐藏画板
 * @param {HTMLButtonElement} drawOpenBtn 绑定事件的元素，现实隐藏
 */
function createListener(drawOpenBtn) {
  // 绑定画板的现实隐藏事件
  drawOpenBtn.addEventListener('click', () => {
    if (drawOpenBtn.textContent === '关闭画板') {
      canvasDraw.classList.add('hide');
      drawOpenBtn.textContent = '🎨';
    } else {
      canvasDraw.classList.toggle('hide');
      drawOpenBtn.textContent = '关闭画板';
    }
  });
}
