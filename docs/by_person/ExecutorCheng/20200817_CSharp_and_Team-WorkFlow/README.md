### 先讲关于GitHub Workflow的一些原则和技巧:

0. 本文里的`PR`全文是`Pull Request`, 在GitLab为`Merge Request`

1. 先讨论好处理`PR`的方式, 是`rebase`还是`merge`

2. 根据代码修改程度决定Reviewer的个数
<details>
<summary>个人小组的一些规则:</summary>

| 是否来自项目维护者 | 更改影响程度 | 需要的批准个数 |
| :----- | :----- | -----: |
| 是 | 小 | 0 |
| 是 | 中 | 0 |
| 是 | 大 | 1 |
| 是 | 重大 | 2 |
| 否 | 小 | 1 |
| 否 | 中 | 1 |
| 否 | 大 | 2 |
| 否 | 重大 | 3 |
</details>

3. 根据commit行为决定分支命名, 比如加一个feature就用`feature/xxxxx`, 修一个bug就用`hotfix/xxxxx`

4. 禁止直接往`master`分支上push, 必须开`PR`

5. 处理PR的时候善用`Review`功能

6. 发现代码中有bug, 或者有新的feature建议的话, 请开一个`Issue`

7. 创建`Issue`之前请检查下有没有重复的

8. 如果一个`PR`是专用于处理一个特定的`Issue`, 可以在`PR`的Description里边加上`fix #Issue编号`来表示它, 当此`PR`被Merge后可以自动关闭对应的`Issue`

9. 可以设置一些`Milestone`, 表示阶段目标, 然后在`Issue`和`PR`里边标上相应的`Milestone`

10. 善用`lable`对`Issue`和`PR`进行分类

### 再说我对C#的一些优点的了解:

0. 0202年了, .NET的跨平台已经比较成熟了, 也一直在持续往8个领域上发展了(`Web`, `Mobile`, `Desktop`, `Microservices`, `Game Development`, `Machine Learning`, `Cloud`, `Internet of Things`)
1. `async`/`await`异步模式: 属于无栈协程, 可以高效率地处理耗时的IO操作
<details>
<summary>详细解答:</summary>

Q: 什么叫`无栈`? A: 异步方法执行过程中所有需要的数据均在堆上

Q: 什么叫`协程`? A: 这是重点。我们都知道, 操作系统切换线程上下文的开销是**非常昂贵**的。那么当有阻塞IO要线程等待的时候, 及时将当前线程的控制权让出去给其它需要执行的代码, 就可以充分运用每个线程被分配到的时间片, 避免多余的上下文切换

Q: 什么时候方法能够继续执行? A: 异步等待中的方法在C#中通过`INotifyCompletion.OnCompleted`或`ICriticalNotifyCompletion.UnsafeOnCompleted`通知异步等待已完成, 并请求线程调度器`TaskScheduler`寻找空闲线程以恢复方法运行

不难看出, 异步模型是解决高并发的首选方案
</details>

2. `Property`: 可以更加简单地对字段操作进行限制
<details>
<summary>详细解答:</summary>
先来看一段代码:

```csharp
public class User
{
    public int Age { get; private set; }
    /* 以下是 `Java` 的约定写法:
    private int _Age;

    public int GetAge() 
    {
        return _Age;
    }

    private void SetAge(int age)
    {
        _Age = age;
    }
    */
}
```

`private set`在这里限制`set`方法的访问级别为`private`

`Property`是一种糖, 封装了对字段的调用, 一体化`getter`和`setter`, 简化及限制对字段的操作

再来看到`List<T>.Capacity`属性, 我们都知道, 一个列表的`Capacity`不能为负, 且不能小于已有的元素个数, 我们来看看BCL是怎么写的

```csharp
public int Capacity
{
    get => _items.Length;
    set
    {
        if (value < _size)
        {
            ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.value, ExceptionResource.ArgumentOutOfRange_SmallCapacity);
        }
 
        if (value != _items.Length)
        {
            if (value > 0)
            {
                T[] newItems = new T[value];
                if (_size > 0)
                {
                    Array.Copy(_items, newItems, _size);
                }
                _items = newItems;
            }
            else
            {
                _items = s_emptyArray;
            }
        }
    }
}
```

在这里, `value`即为即将赋值给`Capacity`的值。那么我们不难发现, `value < _size` 意为如果`Capacity`即将被赋值到小于已有的元素个数时, 马上抛出`ArgumentOutOfRangeException`异常终止操作, 体现了对字段写入的限制作用
</details>

3. 泛型模式: 不仅是编译时, 更有运行时支持。可以更好进行代码复用。`协变`和`逆变`可以更好地支持多种参数需求
<details>
<summary>详细解答:</summary>

#### 先说`协变(Covariance)`:

已知`string`派生自`object`, 那么`string`的派生程度要比`object`高, 协变能使派生程度高的类型隐式转换为派生程度低的类型

那么我们来看到`IEnumerable<out T>`接口 (你可以理解为 `Java` 的 `Iterator<T>`), 它使用`out`关键字声明泛型参数`T`为协变的

那么以下声明合法:

`IEnumerable<object> strobj = new string[0];`

由于`string`派生自`object`, 那么以`object`去迭代`string[]`显然是合法的

与`向上转型(upcasting)`概念结合来看, `object`是`string`的上转型类型, 使用上转型类型返回原始对象也显然是类型安全的

#### 再看`逆变(Contravariance)`:

我们先定义一个委托:`public delegate void PrintHandler<in T>(T value);` (可以理解委托为C++的函数指针`void(*)(T value)`), 它使用`in`关键字声明泛型参数`T`为逆变的

再来随手写个与委托匹配的方法:

```csharp
public void PrintObject(object obj)
{
    Console.WriteLine(obj.ToString());
}
```

不难发现, 将`string`作为`object`传给`PrintObject`是合法的, 于是以下委托的声明合法:

`PrintHandler<string> stringHandler = new PrintHandler<string>(PrintObject)`

再与`向下转型(downcasting)`概念结合来看, `string`是`object`的下转型类型, 使用下转型类型的实例对象作为参数传入显然也是合法的

总结: `协变`只能作用于返回类型, `逆变`只能作用于参数类型。有了这两个变体的存在, 在保留安全性的同时还大大减少了重复代码的编写量
</details>

4. 自定义的`struct`: 更好地控制数据结构, 并且由于其在栈上分配, 通常可以获得比`class`更好的性能
<details>
<summary>详细解答:</summary>

来看到以下两个结构体:

```csharp
// | Red (1 byte) | Green (1 byte) | Blue (1 byte) |
// |              data (3 bytes)                   |
[StructLayout(LayoutKind.Explicit)]
public unsafe struct Rgb24
{
    [FieldOffset(0)]
    public fixed byte data[3];

    [FieldOffset(0)]
    public byte Red;

    [FieldOffset(1)]
    public byte Green;

    [FieldOffset(2)]
    public byte Blue;

    public uint RgbInteger
    {
#if BIGENDIAN
        get => Unsafe.As<Rgb24, uint>(ref this) >> 8;
        /* Assembly
        mov eax, [rcx]
        shr eax, 8
        ret
        */
        set
        {
            ref uint t = ref Unsafe.As<Rgb24, uint>(ref this);
            t = value;
            /* Assembly
            mov [rcx], edx
            ret
            */
        }
#else
        get => BinaryPrimitives.ReverseEndianness(Unsafe.As<Rgb24, uint>(ref this)) >> 8;
        /*
        mov eax, [rcx]
        bswap eax
        shr eax, 8
        ret
        */
        set
        {
            ref uint t = ref Unsafe.As<Rgb24, uint>(ref this);
            t = BinaryPrimitives.ReverseEndianness(value) >> 8;
            /* Assembly
            mov eax, edx
            bswap eax
            shr eax, 8
            mov [rcx], eax
            ret
            */
        }
#endif
    }
}
// | Alpha (1 byte) | Red (1 byte) | Green (1 byte) | Blue (1 byte) |
// |                |                     Rgb24                     |
// |                       data (4 bytes)                           |
// |                           Int32                                |
[StructLayout(LayoutKind.Explicit)]
public unsafe struct Argb32
{
    [FieldOffset(0)]
    public fixed byte data[4];
    
    [FieldOffset(0)]
    private uint _ArgbInteger;

    public uint ArgbInteger // 准备的时候犯错了。将0xFFFFFF00直接赋给它是无法使Alpha值为255的, 由于大小端特性, 需要做一下翻转, 我在这里把它重制为属性
    {
#if BIGENDIAN // 条件编译符, 大端下此块有效, 否则else后的块有效
        get => _ArgbInteger;
        set => _ArgbInteger = value;
#else
        get => BinaryPrimitives.ReverseEndianness(_ArgbInteger);
        set => _ArgbInteger = BinaryPrimitives.ReverseEndianness(value);
#endif
    }

    [FieldOffset(1)]
    public Rgb24 Rgb;

    [FieldOffset(0)]
    public byte Alpha;

    [FieldOffset(1)]
    public byte Red;

    [FieldOffset(2)]
    public byte Green;

    [FieldOffset(3)]
    public byte Blue;
}
```

<details>
<summary>BinaryPrimitives.ReverseEndianness的实现:</summary>

```csharp
public static uint ReverseEndianness(uint value)
{
	// This takes advantage of the fact that the JIT can detect
	// ROL32 / ROR32 patterns and output the correct intrinsic.
	//
	// Input: value = [ ww xx yy zz ]
	//
	// First line generates : [ ww xx yy zz ]
	//                      & [ 00 FF 00 FF ]
	//                      = [ 00 xx 00 zz ]
	//             ROR32(8) = [ zz 00 xx 00 ]
	//
	// Second line generates: [ ww xx yy zz ]
	//                      & [ FF 00 FF 00 ]
	//                      = [ ww 00 yy 00 ]
	//             ROL32(8) = [ 00 yy 00 ww ]
	//
	//                (sum) = [ zz yy xx ww ]
	//
	// Testing shows that throughput increases if the AND
	// is performed before the ROL / ROR.

	return BitOperations.RotateRight(value & 0x00FF00FFu, 8) // xx zz
		+ BitOperations.RotateLeft(value & 0xFF00FF00u, 8); // ww yy
}
public static uint RotateRight(uint value, int offset)
	=> (value >> offset) | (value << (32 - offset));
public static uint RotateLeft(uint value, int offset)
	=> (value << offset) | (value >> (32 - offset));
```
</details>

最好先理解这两个结构体是怎么构成的。从汇编看, 已经优化到和C一个级别了
</details>

5. `ref`/`out`/`in`关键字: 支持按引用而不是按值传递/传出参数, 在对一些较大的`struct`传递上边可以减少内存复制量
> 使用指针传递, 传参在栈上当然用指针了

6. 支持使用指针对数据进行直接操作, 在`C# 9.0`中, 还将支持函数指针的使用

7. 使用`Linq`快速在一组数据中拿到自己想要的结果, 要了解其原理, 得先了解`Enumerator`的工作机制(状态机), 暂时不做赘述
<details>
<summary>详细解答:</summary>

先来看这一组点(X, Y)

```csharp
public static Point[] points = new Point[]
{
    new Point(1, 2),
    new Point(3, 4),
    new Point(5, 6),
    new Point(7, 8),
    new Point(7, 9),
    new Point(9, 10),
    new Point(9, 11)
};
```

Linq的初衷是为了可以用SQL对一组可遍历的数据(只要继承`IEnumerable<out T>`)进行快速查询, 那么

`points.FirstOrDefault(p => p.X == 3)` = `select * from points where X = 3 limit 1`

`points.Any(p => p.X == 3 && p.Y == 4)` = `select 1 from points where X = 3 and Y = 4`

`points.Select(p => p.X)` = `select X from points`

`points.GroupBy(p => p.X)` = `select * from points group by X`

`points.OrderBy(p => p.X)` = `select * from points order by X`

还有其它的就不再赘述了
</details>

如有看法或补充, 欢迎整点Review/Comment
